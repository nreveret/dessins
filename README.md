# Activités de dessins à l'aide de Python

Ce dépôt propose des TP's à destination de collégiens.

On y introduit la programmation impérative en s'appuyant sur des dessins.

Le site est accessible à l'adresse https://nreveret.forge.apps.education.fr/dessins/.
