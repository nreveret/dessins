from js import document  # pour l'insertion de l'image dans le html
import drawsvg as draw

from math import cos, sin, radians


def affiche(div_dessin):
    """Insère le svg de l'imae dans l'élément HTML dont on fournit l'ID"""
    if infos["figure"] is not None:
        division = document.getElementById(div_dessin)
        division.innerHTML = infos["figure"].as_svg()


def nouveau_dessin(largeur, hauteur, avec_bord=False):
    """Crée une nouvelle figure aux dimensions souhaitées"""
    infos.update(INFOS_DEPART)
    infos["figure"] = draw.Drawing(
        largeur,
        hauteur,
        origin="center",
        viewbox=(-largeur // 2, -hauteur // 2, largeur, hauteur),
    )
    if avec_bord:
        lignes(
            -largeur // 2,
            -hauteur // 2,
            -largeur // 2,
            +hauteur // 2,
            +largeur // 2,
            +hauteur // 2,
            +largeur // 2,
            -hauteur // 2,
            -largeur // 2,
            -hauteur // 2,
        )


def couleur_trait(couleur):
    """
    Change la couleur du trait
    La couleur est une chaîne et de caractère et peut être:
    - en texte (en anglais)
    - en rgb(r, g, b)
    - en hexadécimal (au format str)
    """
    infos["svgArgs"]["stroke"] = couleur


def largeur_trait(largeur):
    """
    Change la largeur du trait
    La largeur est un entier positif
    """
    infos["svgArgs"]["stroke_width"] = largeur


def couleur_coloriage(couleur):
    """
    Change la couleur du coloriage
    La couleur est une chaîne et de caractère et peut être:
    - en texte (en anglais)
    - en rgb(r, g, b)
    - en hexadécimal (au format str)
    """
    infos["svgArgs"]["fill"] = couleur


def ligne(x_debut, y_debut, x_fin, y_fin):
    """
    Ajoute la ligne à la figure
    """
    infos["figure"].append(
        draw.Line(x_debut, -y_debut, x_fin, -y_fin, **infos["svgArgs"])
    )


def lignes(*points):
    """
    Ajoute la ligne polygonale à la figure
    On fournit une liste de coordonnées de points [x1, y1, x2, y2, ...]
    """
    if len(points) % 2 == 1:
        raise ValueError("Il faut un nombre pair de coordonnées")
    points_ = [(-1) ** i * v for i, v in enumerate(points)]
    infos["figure"].append(draw.Lines(*points_, **infos["svgArgs"]))


def rectangle(x_debut, y_debut, largeur, hauteur):
    """
    Ajoute le rectangle à la figure
    """
    infos["figure"].append(
        draw.Rectangle(
            x_debut, -y_debut - hauteur, largeur, hauteur, **infos["svgArgs"]
        )
    )


def cercle(x_centre, y_centre, rayon):
    """
    Ajoute le cercle à la figure
    """
    infos["figure"].append(draw.Circle(x_centre, -y_centre, rayon, **infos["svgArgs"]))


def arc(x_centre, y_centre, rayon, angle_debut, angle_fin):
    """
    Ajoute un arc de cercle à la figure
    """
    infos["figure"].append(
        draw.Arc(
            x_centre, -y_centre, rayon, -angle_debut, -angle_fin, **infos["svgArgs"]
        )
    )


def courbe(
    x_debut,
    y_debut,
    x_controle_1,
    y_controle_1,
    x_controle_2,
    y_controle_2,
    x_fin,
    y_fin,
):
    """
    Ajoute une courbe (de Bézier) entre (x_debut, y_debut) et (x_fin, y_fin)
    Les points de contrôles permettent de paramétrer la courbure
    """
    courbe_bezier = draw.Path(**infos["svgArgs"])
    courbe_bezier.M(x_debut, -y_debut)
    courbe_bezier.C(
        x_controle_1, -y_controle_1, x_controle_2, -y_controle_2, x_fin, -y_fin
    )
    infos["figure"].append(courbe_bezier)


def texte(contenu, x, y, taille):
    """
    Ajoute le texte 'contenu' à la position (x, y) et à la taille donnée
    """
    infos["figure"].append(draw.Text(contenu, taille, x, -y, **infos["svgArgs"]))


def sauvegarde_SVG(nom):
    """
    Sauvegarde le fichier au format svg dans le dossier courant
    L'extension "svg" est ajoutée automatiquement
    figure.sauvegarde("image") crée, ou modifie, le fichier image.svg
    """
    infos["figure"].saveSvg(nom + ".svg")


def sauvegarde_PNG(nom):
    """
    Sauvegarde le fichier au format png dans le dossier courant
    L'extension "png" est ajoutée automatiquement
    figure.sauvegarde("image") crée, ou modifie, le fichier image.png
    """
    infos["figure"].savePng(nom + ".png")


def en_texte():
    """
    Renvoie le contenu html de la figure
    """
    return infos["figure"].as_svg()


def avance(pixels):
    """Avance et trace un trait long de "pixels" px"""
    nouveau_x = infos["x"] + pixels * cos(radians(infos["angle"]))
    nouveau_y = infos["y"] + pixels * sin(radians(infos["angle"]))
    ligne(infos["x"], infos["y"], nouveau_x, nouveau_y)
    infos["x"] = nouveau_x
    infos["y"] = nouveau_y
    if infos["mode_coloriage"]:
        infos["points"].extend([infos["x"], infos["y"]])


def gauche(degres):
    """Tourne à gauche de "degres" degrés"""
    infos["angle"] += degres
    infos["angle"] %= 360


def droite(degres):
    """Tourne à droite de "degres" degrés"""
    infos["angle"] -= degres
    infos["angle"] %= 360


def saute(x, y):
    """Amène ne crayon à la position x, y SANS DESSINER"""
    infos["x"] = x
    infos["y"] = y
    if infos["mode_coloriage"]:
        infos["points"].extend([x, y])


def debut_coloriage():
    """Débute une séquence de coloriage avec la tortue"""
    infos["mode_coloriage"] = True
    infos["points"] = [infos["x"], infos["y"]]


def fin_coloriage():
    """Termine une séquence de coloriage avec la tortue"""
    ancienne_couleur = infos["svgArgs"]["stroke"]
    infos["svgArgs"]["stroke"] = "transparent"
    lignes(*infos["points"])
    infos["svgArgs"]["stroke"] = ancienne_couleur
    infos["mode_coloriage"] = False


def orientation():
    """
    Renvoie l'orientation de la tortue en degrés
    0° : droite ; 90° : haut ; 180° : gauche ; 270° : bas
    """
    return infos["angle"]


def oriente_vers(degres):
    """
    Oriente la tortue vers l'orientation fournie
    0° : droite ; 90° : haut ; 180° : gauche ; 270° : bas
    """
    infos["angle"] = degres


def position():
    """
    Renvoie la position (x, y) de la tortue
    Les coordonnées récupérées peuvent être décimales
    du fait des calculs trigonométriques liés au dessin
    """
    return infos["x"], infos["y"]


INFOS_DEPART = {
    "figure": None,
    "svgArgs": {"stroke_width": 1, "stroke": "black", "fill": "transparent"},
    "x": 0,
    "y": 0,
    "points": [],
    "angle": 0,
    "mode_coloriage": False,
}

infos = {}
