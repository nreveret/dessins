---
author: Nicolas Revéret
title: Memento
---

# 🧐 Les instructions 🧐

??? question "En ligne ou en local ?"

    Le module de dessin (nommé `dessin_svg`) utilisé dans ce site peut aussi être utilisé localement.

    Il suffit de le télécharger en [cliquant ici](dessin_svg.py) et de le placer dans le dossier de travail :
        
    ``` text title=""
    dossier_de_travail/
        |__ dessin_svg.py
        |__ rectangle_jaune.py
    ```
    
    Il faut aussi que le module `drawsvg` soit installé :

    === "Avec Windows"

        Ouvrir l'invite de commande `cmd` et saisir `pip install drawsvg` ou `python -m pip install drawsvg`.

    === "Avec Linux ou MacOs"

        Ouvrir un terminal saisir `pip3 install drawsvg`.

    On peut alors créer un fichier `Python` et y saisir :

    ```python title="Contenu de <code>rectangle_jaune.py</code>"
    from dessin_svg import *  # Indispensable pour importer toutes les fonctions !

    nouveau_dessin(500, 400)
    couleur_trait("yellow")
    rectangle(0, 0, 100, 70)
    affiche()
    ```

## :pencil: Créer, paramétrer, afficher

??? tip "Créer un `nouveau_dessin`"

    Il faut fournir deux paramètres :

    * `largeur` : la largeur de l'image en pixels ;
    
    * `hauteur` : la hauteur de l'image en pixels.
    
    Par exemple, `#!py nouveau_dessin(500, 800)` crée figure de 500 pixels de large et 800 de haut.

??? tip "Régler la `couleur_trait`"

    Cette fonction permet de régler la couleur du tracé pour tous les objets.

    Il faut fournir un seul paramètre : la `couleur`, entre guillemets.

    Cette couleur peut être :

    * un nom, par exemple `"blue"` ou `"yellow` ;
    * un code en *(rouge, vert, bleu)*, par exemple `#!py "rgb(255, 0, 255)"` pour du rose ;
    * un code en *hexadécimal*, par exemple `#!py "#ff00ff"` pour du rose.

    Par exemple, `#!py couleur_trait("blue")` règle la couleur du trait à *bleu*.

    Voir [cette page](https://www.w3schools.com/colors/colors_names.asp) pour des détails.

    **On peut aussi faire `#!py couleur_trait("transparent")` pour ne pas écrire.**

??? tip "Régler la `couleur_coloriage`"

    Cette fonction permet de régler la couleur du coloriage des figures, fermées ou non !

    Il faut fournir un seul paramètre : la `couleur`, entre guillemets.

    Cette couleur peut être :

    * un nom, par exemple `"blue"` ou `"yellow` ;
    * un code en *(rouge, vert, bleu)*, par exemple `#!py "rgb(255, 0, 255)"` pour du rose ;
    * un code en *hexadécimal*, par exemple `#!py "#ff00ff"` pour du rose.

    Par exemple, `#!py couleur_coloriage("yellow")` règle la couleur du coloriage à *jaune*.

    Voir [cette page](https://www.w3schools.com/colors/colors_names.asp) pour des détails.

    **On peut aussi faire `#!py couleur_coloriage("transparent")` pour ne plus colorier.**

??? tip "Régler la `largeur_trait`"

    Cette fonction permet de régler la largeur du trait en pixels.

    Il faut fournir un seul paramètre :

    * `largeur` : la taille souhaitée, un nombre entier positif.

    Par exemple, `#!py largeur_trait(6)` règle la largeur du trait à 6 pixels.

??? tip "`affiche` l'image"

    Cette fonction permet d'afficher l'image à l'écran. Elle est indispensable si l'on voir le résultat.

    Il n'y a pas de paramètre.

    Par exemple, `#!py affiche()` va demander à `Python` d'afficher l'image.

??? tip "`sauvegarde_SVG` l'image (ne fonctionne qu'en local)"

    Cette fonction permet de sauvegarder l'image au format `SVG`. 

    Il faut fournir un seul paramètre : le nom du fichier. Il est inutile de saisir l'extension.

    Par exemple, `#!py sauvegarde_SVG("super_dessin")` va enregistrer l'image dans `super_dessin.svg`.

??? tip "`sauvegarde_PNG` l'image (ne fonctionne qu'en local)"

    Cette fonction permet de sauvegarder l'image au format `PNG`. 

    Il faut fournir un seul paramètre : le `nom` du fichier. Il est inutile de saisir l'extension.

    Par exemple, `#!py sauvegarde_P?G("super_dessin")` va enregistrer l'image dans `super_dessin.png`.


## :pencil2: Dessiner

??? tip "Tracer une `ligne`"

    Trace un segment sur la figure.

    Il faut fournir quatre paramètres :

    * `x_debut` : l'abscisse du point de départ ;
    * `y_debut` : l'ordonnée du point de départ ;
    * `x_fin` : l'abscisse du point d'arrivée ;
    * `y_fin` : l'ordonnée du point de départ.

    On peut régler la largeur du trait ou sa couleur.

    Par exemple, `#!py ligne(-30, 50, 20, 10)` trace une ligne depuis le point de coordonnées `#!py x = -30` et `#!py y = 50` vers celui de coordonnées `#!py x = 20` et `#!py y = 10`.

??? tip "Tracer des `#!py lignes`"

    Trace une suite de segments sur la figure.

    Il faut fournir les coordonnées `x` et `y` des différents points, à la suite les uns des autres.

    Si l'on a réglé la couleur de coloriage, la zone entourée par les lignes est coloriée.

    Par exemple, `#!py lignes(-30, 50, 20, 10, 100, 30)` trace la ligne :

    * partant de `#!py -30, 50`,
    * passant par `#!py 20, 10`,
    * se terminant en `#!py 100, 30`.

??? tip "Tracer un `rectangle`"

    Trace un rectangle sur la figure.

    Il faut fournir quatre paramètres :

    * `x` : l'abscisse du sommet en bas à gauche ;
    * `y` : l'ordonnée du sommet en bas à gauche ;
    * `largeur` : la largeur du rectangle (nombre entier positif) ;
    * `hauteur` : la hauteur du rectangle (nombre entier positif).

    Par exemple, `#!py rectangle(-30, 50, 20, 10)` trace le rectangle dont le sommet en bas à gauche est en `#!py -30, 50` et faisant `#!py 20` pixels de large sur `#!py 10` de haut.

??? tip "Tracer un `cercle`"

    Trace un cercle sur la figure.

    Il faut fournir trois paramètres :

    * `x_centre` : l'abscisse du centre ;
    * `y_centre` : l'ordonnée du centre ;
    * `rayon` : le rayon du cercle (nombre entier positif).

    Par exemple, `#!py cercle(-30, 50, 200)` trace le cercle dont le centre est en `#!py -30, 50` et ayant un rayon de`#!py 200` pixels.

??? tip "Tracer un `arc` de cercle"

    Trace un arc de cercle sur la figure.

    Il faut fournir cinq paramètres :

    * `x_centre` : l'abscisse du centre ;
    * `y_centre` : l'ordonnée du centre ;
    * `rayon` : le rayon du cercle (nombre entier positif) ;
    * `angle_debut` : l'angle où débute l'arc (nombre entier) ;
    * `angle_fin` : l'angle où se termine l'arc (nombre entier) ;

    Les angles de début et de fin sont mesurés en degré. L'angle `#!py 0` est à droite et l'on tourne dans l'ordre inverse des aiguilles d'une montre. Ainsi, l'angle `#!py 90` est en haut, `#!py 270` en bas.
    
    Par exemple, `#!py arc(-30, 50, 200, -90, 180)` trace l'arc de cercle dont le centre est en `#!py -30, 50`, ayant un rayon de `#!py 200` pixels et partant du bas (`#!py -90`) pour aller jusqu'à la gauche (`#!py 180`). On fait donc trois-quarts de tour.

??? tip "Tracer un `courbe`"

    Trace une courbe sur la figure.

    Il faut fournir six paramètres :

    * `x_debut` : l'abscisse du point de départ ;
    * `y_debut` : l'ordonnée du point de départ ;
    * `x_controle_1` : l'abscisse du premier point de contrôle ;
    * `y_controle_1` : l'ordonnée du premier point de contrôle ;
    * `x_controle_2` : l'abscisse du second point de contrôle ;
    * `y_controle_2` : l'ordonnée du second point de contrôle ;
    * `x_fin` : l'abscisse du point d'arrivée ;
    * `y_fin` : l'ordonnée du point d'arrivée.

    Les points de contrôle permettent de préciser la courbure du tracé ([source de l'image](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths)).

    ![Points de contrôle](../aide/bezier.png){.center width=25%}

    Par exemple, `#!py courbe(-30, 50, 50, 100, -50, 30, 40, 100)` trace une courbe entre les points de coordonnées `#!py -30, 50` et `#!py 40, 100`. Le premier point de contrôle est en `#!py 50, 100`, le second en `#!py -50, 30`.

## :turtle: Le mode « *tortue* »

Dans ce mode, on imagine qu'une tortue tient le crayon ! Pour dessiner, il faut alors tout lui dire : « *avance de 100 pixels* », « *tourne à droite de 45°* », *etc*.


??? tip "`avance` de ..."

    Avance et trace un trait.

    Il faut fournit un seul paramètre : la longueur `pixels` (un nombre entier).

    Par exemple, `#!py avance(152)` fait avancer le crayon de 152 pixels.

??? tip "Tourne vers la `gauche` de ..."

    Tourne à gauche d'un certain nombre de degrés.

    Il faut fournit un seul paramètre : l'angle `degres`.

    Initialement le crayon point vers la gauche. On tourne dans le sens inverse des aiguille d'une montre (90 est donc en haut).

    Par exemple, `#!py gauche(90)` tourne le crayon de 90° vers la gauche. S'il pointait vers la droite, il pointe désormais vers le haut.
    

??? tip "Tourne vers la `droite` de ..."

    Tourne à droite d'un certain nombre de degrés.

    Il faut fournit un seul paramètre : l'angle `degres`.

    Initialement le crayon point vers la gauche. On tourne dans le sens inverse des aiguille d'une montre (90 est donc en haut).

    Par exemple, `#!py droite(90)` tourne le crayon de 90° vers la droite. S'il pointait vers la droite, il pointe désormais vers le bas.


??? tip "`saute` jusqu'à ..."

    Amène ne crayon à la position x, y **sans dessiner**.

    Il faut fournit deux paramètres :
    
    * l'abscisse `x` du point d'arrivée,
    * l'ordonnée `y` du point d'arrivée.

    Par exemple, `#!py saute(-20, 100)` amène le crayon en `x = -20, y = 100`.


??? tip "`oriente_vers` ..."

    Oriente la tortue vers l'angle fournit en paramètres.

    Il faut fournir l'angle (en degrés) en paramètre.
    
    Par exemple, `#!py oriente_vers(90)` oriente la tortue vers le haut.


??? tip "`debut_coloriage`"

    Débute une séquence de coloriage avec la tortue. Tous les tracés entre l'appel de cette fonction et 
    celui de `fin_coloriage` formeront une unique forme coloriée.

    Il n'y a pas de paramètre. La couleur du coloriage est réglée avec `couleur_coloriage`.

    Par exemple, `#!py debut_coloriage()` débute une séquence de coloriage.


??? tip "`fin_coloriage`"

    Termine une séquence de coloriage avec la tortue.

    Il n'y a pas de paramètre.
    
    Par exemple, `#!py fin_coloriage()` termine la séquence de coloriage.


??? tip "`position`"

    Renvoie la position de la tortue sous la forme d'un couple de coordonnées `#!py (x, y)`.

    Il n'y a pas de paramètre.


??? tip "`orientation`"

    Renvoie l'orientation de la tortue.

    Il n'y a pas de paramètre.
