---
author: Nicolas Revéret
title: Répétitions
---

# :robot: Répéter des actions

## 🙀 Problème... Solution 😺

Nous savons construire un carré :

![Carré vert](../4_repetitions/images/carre.svg){ align=right }


```python
nouveau_dessin(400, 400)
couleur_coloriage("green")
debut_coloriage()
gauche(90)  # un côté
avance(150)
gauche(90)  # deux côtés
avance(150)
gauche(90)  # trois côtés
avance(150)
gauche(90)  # quatre côtés
avance(150)
fin_coloriage()
```

Cela fonctionne mais heureusement que l'on ne souhaite pas construire un [*myriagone*](https://fr.wikipedia.org/wiki/Myriagone) à 1 000 côtés ! 

`Python` a la solution : la boucle `for` !

Réfléchissons... dessiner un carré demande d'effectuer quatre fois les mêmes actions :

* tourner de 90°,
* avancer d'une certaine longueur.

Si l'on doit expliquer à quelqu'un comment faire nous lui dirons :

```text title="📋 Explications"
Prends une feuille
Prends un crayon vert

Répète 4 fois :
    Tourne ton crayon de 90°
    Trace un trait de 150 pixels de long

Colorie l'intérieur
Contemple ta figure
```

Avec `Python`, on peut faire cela facilement :

```python
nouveau_dessin(400, 400)
couleur_coloriage("green")
debut_coloriage()

for repetition in range(4):  # répète 4 fois les actions décalées
    gauche(90)
    avance(150)

fin_coloriage()
```
Le code est plus court, plus clair... Plus facile à écrire !

{{ IDE('./pythons/repetitions', MODE="no_valid", MAX=10, TERM_H=2, MIN_SIZE= 10)}}

{{ figure('repetitions',
    div_class='py_mk_figure center',
    inner_text= 'Votre figure sera ici',
    admo_kind='???+',
    admo_class='tip',
    admo_title='Votre figure',
) }}

## À vous de jouer !

??? question "Octogone 8️⃣"

    Modifiez le code précédent afin de construire un octogone de 50 pixels de côté.

    On rappelle que l'octogone a huit côtés :wink:.

    {{ IDE('./pythons/octogone', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('octogone',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Pentagone 5️⃣"

    Modifiez le code afin de construire un pentagone de 50 pixels de côté.

    On rappelle que le pentagone a cinq côtés :wink:.

    {{ IDE('./pythons/pentagone', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('pentagone',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Soleil 🌞"

    Le soleil ci-dessous est dans une image de 200 sur 200.
    
    Il est constitué de 18 segments mesurant chacun 100 pixels.

    ![Soleil](images/soleil.svg){.center}

    Chaque trait a une largeur de 5 pixels.
    
    Tracer le soleil.

    {{ IDE('./pythons/soleil', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('soleil',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Triangles 🚩"

    La figure ci-dessous a l'air très compliquée...

    ![Hélice](images/triangles.svg){.center}
    
    En fait il s'agit de 30 triangles équilatéraux ayant tous des côtés mesurant 100 pixels.

    Les instructions pour la tracer sont donc :

    ```text title="📋 Explications"
    Crée une figure de 300 par 300
    Prends un crayon de couleur bleue

    Répète 30 fois :
        Tourne à gauche de 12°
        Répète 3 fois :
            Tourne à gauche de 120°
            Trace un trait de 100 pixels de long
    ```

    Tracer la figure.


    {{ IDE('./pythons/triangles', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('triangles',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Hélice 🎡"

    On souhaite dessiner cette figure :

    ![Hélice](images/helice.svg){.center}
    
    On y trouve 12 branches :
    
    * chacune débute par un segment de 100 pixels,
    * chacune se termine par un triangle équilatéral de 50 pixels de côté

    Les segments sont tous en jaune et les triangles sont coloriés en vert.

    Tracer la figure.

    On donne ci-dessous une aide sous la forme d'un code à trous :

    ??? tip "Aide"
    
        ```python
        nouveau_dessin(300, 300)
        couleur_trait("...")
        couleur_coloriage("...")

        for repetition in range(...):  # Une répétition par branche
            gauche(...)
            avance(...)
            debut_coloriage()
            for cote in range(...):  # tracé d'un triangle
                gauche(...)
                avance(...)
            fin_coloriage()
            saute(0, 0)

        affiche()
        ```    


    {{ IDE('./pythons/helice', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('helice',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Défi 🕸"

    Dessiner une toile d'araignée :

    ![Toile d'araignée](images/toile.svg){.center .autolight}

    Le plus simple est de faire une toile à 6 branches.
    
    Ce coup-ci il n'y a pas de solution ! 😲
    
    {{ IDE('./pythons/defi', MODE="no_valid", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('defi',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}