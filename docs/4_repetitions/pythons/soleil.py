# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(200, 200)

...
# --- PYODIDE:corr --- #
nouveau_dessin(200, 200)
couleur_trait("yellow")
largeur_trait(5)

for repetition in range(18):
    gauche(20)
    avance(100)
    saute(0, 0)
# --- PYODIDE:post --- #
affiche("soleil")
