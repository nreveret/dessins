# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(300, 300)
couleur_trait("...")
couleur_coloriage("...")

for repetition in range(...):  # Une répétition par branche
    gauche(...)
    avance(...)
    debut_coloriage()
    for cote in range(...):  # tracé d'un triangle
        gauche(...)
        avance(...)
    fin_coloriage()
    saute(0, 0)
# --- PYODIDE:corr --- #
nouveau_dessin(300, 300)
couleur_trait("yellow")
couleur_coloriage("green")

for repetition in range(12):  # Une répétition par branche
    gauche(30)
    avance(100)
    debut_coloriage()
    for cote in range(3):  # tracé d'un triangle
        gauche(120)
        avance(50)
    fin_coloriage()
    saute(0, 0)
# --- PYODIDE:post --- #
affiche("helice")
