# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(200, 200)

...
# --- PYODIDE:corr --- #
nouveau_dessin(300, 300)
largeur_trait(1)
couleur_trait("blue")

for repetition in range(30):
    gauche(12)
    for cote in range(3):
        gauche(120)
        avance(100)

# --- PYODIDE:post --- #
affiche("triangles")
