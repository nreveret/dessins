# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(400, 400)

for repetition in range(...):
    ...
# --- PYODIDE:corr --- #
nouveau_dessin(400, 400)
couleur_coloriage("green")
debut_coloriage()

for repetition in range(8):
    gauche(45)
    avance(50)

fin_coloriage()
# --- PYODIDE:post --- #
affiche("octogone")
