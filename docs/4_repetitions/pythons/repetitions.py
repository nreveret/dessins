# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(400, 400)
couleur_coloriage("green")
debut_coloriage()

for repetition in range(4):  # répète 4 fois les actions décalées
    gauche(90)
    avance(150)

fin_coloriage()
# --- PYODIDE:post --- #
affiche("repetitions")
