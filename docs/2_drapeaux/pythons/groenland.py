# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(360, 240)
...
# --- PYODIDE:corr --- #
nouveau_dessin(360, 240)
# La bande blanche
couleur_trait("#FFFFFF")
couleur_coloriage("#FFFFFF")
rectangle(-180, 0, 360, 120)
# La bande rouge
couleur_trait("#FF0000")
couleur_coloriage("#FF0000")
rectangle(-180, -120, 360, 120)
# Le demi-disque blanc
couleur_trait("#FFFFFF")
couleur_coloriage("#FFFFFF")
arc(-40, 0, 80, 180, 360)
# Le demi-disque rouge
couleur_trait("#FF0000")
couleur_coloriage("#FF0000")
arc(-40, 0, 80, 0, 180)
# --- PYODIDE:post --- #
affiche("groenland")
