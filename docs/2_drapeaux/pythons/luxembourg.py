# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(500, 300)
...
# --- PYODIDE:corr --- #
nouveau_dessin(500, 300)
# le rouge
couleur_trait("#C73F4A")
couleur_coloriage("#C73F4A")
rectangle(-250, 50, 500, 100)
# le blanc
couleur_trait("#FFFFFF")
couleur_coloriage("#FFFFFF")
rectangle(-250, -50, 500, 100)
# le bleu
couleur_trait("#0089B6")
couleur_coloriage("#0089B6")
rectangle(-250, -150, 500, 100)
# --- PYODIDE:post --- #
affiche("luxembourg")
