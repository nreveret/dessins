# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)
...
# --- PYODIDE:corr --- #
nouveau_dessin(360, 240)
# Le rouge
couleur_trait("#A51931")
couleur_coloriage("#A51931")
rectangle(-180, -120, 600, 240)
# Le blanc
couleur_trait("#F4F5F8")
couleur_coloriage("#F4F5F8")
rectangle(-180, -80, 360, 40)
rectangle(-180, 40, 360, 40)
# Le bleu
couleur_trait("#2D2A4A")
couleur_coloriage("#2D2A4A")
rectangle(-180, -40, 360, 80)
# --- PYODIDE:post --- #
affiche("thailande")
