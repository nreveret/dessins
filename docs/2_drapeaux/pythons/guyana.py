# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(500, 300)
...
# --- PYODIDE:corr --- #
nouveau_dessin(500, 300)
couleur_coloriage("green")
rectangle(-250, -150, 500, 300)
largeur_trait(10)
couleur_trait("white")
couleur_coloriage("yellow")
lignes(-250, 150, 235, 0, -250, -150)
couleur_trait("black")
couleur_coloriage("red")
lignes(-250, 150, -15, 0, -250, -150)
# --- PYODIDE:post --- #
affiche("guyana")
