# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(300, 200)
...
# --- PYODIDE:corr --- #
nouveau_dessin(300, 200)
# Le blanc
couleur_trait("#FFFFFF")
couleur_coloriage("#FFFFFF")
rectangle(-150, -100, 300, 200)
# Le disque rouge
couleur_trait("#BC002D")
couleur_coloriage("#BC002D")
cercle(0, 0, 60)
# --- PYODIDE:post --- #
affiche("japon")
