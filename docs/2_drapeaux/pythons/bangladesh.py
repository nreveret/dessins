# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(500, 300)
...
# --- PYODIDE:corr --- #
nouveau_dessin(500, 300)
# Le vert
couleur_trait("#006A4E")
couleur_coloriage("#006A4E")
rectangle(-250, -150, 500, 300)
# Le disque rouge
couleur_trait("#F42A41")
couleur_coloriage("#F42A41")
cercle(-15, 0, 100)
# --- PYODIDE:post --- #
affiche("bangladesh")
