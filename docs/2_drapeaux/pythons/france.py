# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)
...
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)
# le bleu
couleur_trait("#2c325c")
couleur_coloriage("#2c325c")
rectangle(-300, -200, 200, 400)
# le blanc
couleur_trait("#f3ebf2")
couleur_coloriage("#f3ebf2")
rectangle(-100, -200, 200, 400)
# le rouge
couleur_trait("#af2030")
couleur_coloriage("#af2030")
rectangle(100, -200, 200, 400)
# --- PYODIDE:post --- #
affiche("france")
