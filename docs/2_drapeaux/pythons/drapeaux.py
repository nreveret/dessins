# --- HDR ---#
from js import document  # pour l'insertion de l'image dans le html
from math import cos, sin, radians

try:
    import drawSvg as draw
except ImportError:
    # Import de drawSvg
    import pyodide_js

    await pyodide_js.loadPackage("micropip")
    import micropip

    await micropip.install("drawsvg==2.0.2")

    # Fonctions de desin
    import drawsvg as draw


def affiche():
    division = document.getElementById(div_dessin)
    division.innerHTML = _figure.as_svg()


def nouveau_dessin(largeur, hauteur):
    global _figure
    _figure = draw.Drawing(
        largeur,
        hauteur,
        origin="center",
        viewbox=(-largeur // 2, -hauteur // 2, largeur, hauteur),
    )


def couleur_trait(couleur):
    """
    Change la couleur du trait
    La couleur est une chaîne et de caractère et peut être:
    - en texte (en anglais)
    - en rgb(r, g, b)
    - en hexadécimal (au format str)
    """
    _svgArgs["stroke"] = couleur


def largeur_trait(largeur):
    """
    Change la largeur du trait
    La largeur est un entier positif
    """
    _svgArgs["stroke_width"] = largeur


def couleur_coloriage(couleur):
    """
    Change la couleur du coloriage
    La couleur est une chaîne et de caractère et peut être:
    - en texte (en anglais)
    - en rgb(r, g, b)
    - en hexadécimal (au format str)
    """
    _svgArgs["fill"] = couleur


def ligne(x_debut, y_debut, x_fin, y_fin):
    """
    Ajoute la ligne à la figure
    """
    _figure.append(draw.Line(x_debut, -y_debut, x_fin, -y_fin, **_svgArgs))


def lignes(*points):
    """
    Ajoute la ligne polygonale à la figure
    On fournit une liste de coordonnées de points [x1, y1, x2, y2, ...]
    """
    if len(points) % 2 == 1:
        raise ValueError("Il faut un nombre pair de coordonnées")
    points_ = [(-1)**i * v for i, v in enumerate(points)]
    _figure.append(draw.Lines(*points_, **_svgArgs))


def rectangle(x_debut, y_debut, largeur, hauteur):
    """
    Ajoute le rectangle à la figure
    """
    _figure.append(draw.Rectangle(x_debut, -y_debut - hauteur, largeur, hauteur, **_svgArgs))


def cercle(x_centre, y_centre, rayon):
    """
    Ajoute le cercle à la figure
    """
    _figure.append(draw.Circle(x_centre, -y_centre, rayon, **_svgArgs))


def arc(x_centre, y_centre, rayon, angle_debut, angle_fin):
    """
    Ajoute un arc de cercle à la figure
    """
    _figure.append(draw.Arc(x_centre, -y_centre, rayon, -angle_debut, -angle_fin, **_svgArgs))


def courbe(x_debut, y_debut, x_controle_1, y_controle_1, x_controle_2, y_controle_2, x_fin, y_fin):
    """
    Ajoute une courbe (de Bézier) entre (x_debut, y_debut) et (x_fin, y_fin)
    Les points de contrôles permettent de paramétrer la courbure
    """
    courbe_bezier = draw.Path(**_svgArgs)
    courbe_bezier.M(x_debut, -y_debut)
    courbe_bezier.C(x_controle_1, -y_controle_1, x_controle_2, -y_controle_2, x_fin, -y_fin)
    _figure.append(courbe_bezier)


def texte(contenu, x, y, taille):
    """
    Ajoute le texte 'contenu' à la position (x, y) et à la taille donnée
    """
    _figure.append(draw.Text(contenu, taille, x, -y, **_svgArgs))


def sauvegarde_SVG(nom):
    """
    Sauvegarde le fichier au format svg dans le dossier courant
    L'extension "svg" est ajoutée automatiquement
    figure.sauvegarde("image") crée, ou modifie, le fichier image.svg
    """
    _figure.saveSvg(nom + ".svg")


def sauvegarde_PNG(nom):
    """
    Sauvegarde le fichier au format png dans le dossier courant
    L'extension "png" est ajoutée automatiquement
    figure.sauvegarde("image") crée, ou modifie, le fichier image.png
    """
    _figure.savePng(nom + ".png")


def en_texte():
    """
    Renvoie le contenu html de la figure
    """
    return _figure.as_svg()


def avance(pixels):
    """Avance et trace un trait long de "pixels" px"""
    nouveau_x = _x + pixels * cos(radians(_angle))
    nouveau_y = _y + pixels * sin(radians(_angle))
    ligne(_x, _y, nouveau_x, nouveau_y)
    globals()["_x"] = nouveau_x
    globals()["_y"] = nouveau_y
    if _mode_coloriage:
        globals()["_points"].extend([_x, _y])


def gauche(degres):
    """Tourne à gauche de "degres" degrés"""
    globals()["_angle"] += degres


def droite(degres):
    """Tourne à droite de "degres" degrés"""
    globals()["_angle"] -= degres



def saute(x, y):
    """Amène ne crayon à la position x, y SANS DESSINER"""
    globals()["_x"] = x
    globals()["_y"] = -y
    if _mode_coloriage:
        globals()["_points"].extend([_x, _y])


def debut_coloriage():
    """Débute une séquence de coloriage avec la tortue"""
    globals()["_mode_coloriage"] = True
    globals()["_points"] = [_x, _y]


def fin_coloriage():
    """Termine une séquence de coloriage avec la tortue"""
    ancienne_couleur = _svgArgs["stroke"]
    _svgArgs["stroke"] = "transparent"
    lignes(*_points)
    _svgArgs["stroke"] = ancienne_couleur
    globals()["_mode_coloriage"] = False


_figure = None
_svgArgs = {"stroke_width": 1, "stroke": "black", "fill": "white"}
_x = 0
_y = 0
_points = []
_angle = 0
_mode_coloriage = False

div_dessin = "drapeaux"
# --- HDR ---#
nouveau_dessin(600, 400)
affiche()
