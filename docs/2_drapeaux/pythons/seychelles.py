# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(300, 150)
...
# --- PYODIDE:corr --- #
nouveau_dessin(300, 150)
# La bande bleue
couleur_trait("blue")
couleur_coloriage("blue")
lignes(-150, -75, -150, 75, -50, 75)
# La bande jaune
couleur_trait("yellow")
couleur_coloriage("yellow")
lignes(-150, -75, -50, 75, 50, 75)
# La bande rouge
couleur_trait("red")
couleur_coloriage("red")
lignes(-150, -75, 50, 75, 150, 75, 150, 25)
# La bande blanche
couleur_trait("white")
couleur_coloriage("white")
lignes(-150, -75, 150, -25, 150, 25)
# La bande verte
couleur_trait("green")
couleur_coloriage("green")
lignes(-150, -75, 150, -25, 150, -75)
# --- PYODIDE:post --- #
affiche("seychelles")
