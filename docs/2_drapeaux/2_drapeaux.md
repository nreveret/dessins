---
author: Nicolas Revéret
title: Drapeaux
---

# 🚩 Tracer des drapeaux

Nous avons vu dans la page précédente comment construire nos premières figures.
Vous pouvez à tout moment consulter [l'aide](../aide/aide.md) afin d'avoir un rappel des fonctions possibles.

Dans cette page nous allons dessiner des drapeaux. Des simples, des carrés, des compliqués !

Les drapeaux cités ont tous une page Wikipedia. Vous pourrez souvent y trouver les « vraies » couleurs si vous êtes perfectionnistes :wink:. Les images de cette pages proviennent d'ailleurs toutes de Wikipedia.

## Avec des bandes

??? question "France :flag_fr:"

    Vous connaissez bien entendu le drapeau français !

    ![Le drapeau français](images/france.svg){.center width=30%}

    On précise que le drapeau est au format 3:2 (si la largeur mesure 3 unités, la hauteur en mesure 2).

    Dessinez-le !
    
    {{ IDE('./pythons/france', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('france',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}


??? question "Luxembourg :flag_lu:"

    Le drapeau du Luxembourg est au format 5:3.

    ![Le drapeau luxembourgeois](images/luxembourg.svg){.center width=30%}

    Ses couleurs sont les suivantes :

    |   Couleur    | Code hexadécimal |
    | :----------: | :--------------: |
    | Rouge fraise |    `#C73F4A`     |
    |  Blanc pur   |    `#FFFFFF`     |
    |  Bleu clair  |    `#0089B6`     |

    Vous pouvez directement les copier dans le code `Python` en les mettant entre guillemets.

    {{ IDE('./pythons/luxembourg', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('luxembourg',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}


??? question "Thaïlande :flag_th:"

    Le drapeau de la Thaïlande est au format 3:2.

    ![Le drapeau thaïlandais](images/thailande.svg){.center width=30%}

    Les largeurs des bandes sont 1:1:2:1:1.

    {{ IDE('./pythons/thailande', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('thailande',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

## Avec des cercles

Avant d'aller plus loin, introduisons une nouvelle instruction : le `cercle`.

???+ tip "Tracer un `cercle`"

    Trace un cercle sur la figure.

    Il faut fournir trois paramètres :

    * `x_centre` : l'abscisse du centre ;
    * `y_centre` : l'ordonnée du centre ;
    * `rayon` : le rayon du cercle (nombre entier positif).

    Par exemple, `#!py cercle(-30, 50, 200)` trace le cercle dont le centre est en `#!py -30, 50` et ayant un rayon de`#!py 200` pixels.

??? question "Japon :flag_jp:"

    Le drapeau du Japon est au format 3:2.

    ![Le drapeau japonais](images/japon.svg){width=30% .center}

    Le disque central a un rayon égal au $\frac{3}{10}$ de la hauteur.
    
    ![Les dimensions du drapeau japonais](images/japon_construction.svg){width=36% .center}

    {{ IDE('./pythons/japon', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('japon',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Bangladesh :flag_bd:"

    Le drapeau du Bangladesh est au format 5:3.

    ![Le drapeau bangladais](images/bangladesh.svg){width=30% .center}

    Le disque central a un rayon égal au $\frac{1}{3}$ de la hauteur. Son centre est un peu décalé vers la gauche.

    ![Les dimensions du drapeau bangladais](images/bangladesh_construction.svg){width=33% .autolight .center}

    {{ IDE('./pythons/bangladesh', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('bangladesh',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

Avant de tracer le drapeau groenlandais, découvrons l'instruction `arc` :

???+ tip "Tracer un `arc` de cercle"

    Trace un arc de cercle sur la figure.

    Il faut fournir cinq paramètres :

    * `x_centre` : l'abscisse du centre ;
    * `y_centre` : l'ordonnée du centre ;
    * `rayon` : le rayon du cercle (nombre entier positif) ;
    * `angle_debut` : l'angle où débute l'arc (nombre entier positif) ;
    * `angle_fin` : l'angle où se termine l'arc (nombre entier positif) ;

    Les angles de début et de fin sont mesurés en degré. L'angle `#!py 0` est à droite et l'on tourne dans l'ordre inverse des aiguilles d'une montre. Ainsi, l'angle `#!py 90` est en haut, `#!py 270` en bas.
    
    Par exemple, `#!py arc(-30, 50, 200, 180, 360)` trace l'arc de cercle dont le centre est en `#!py -30, 50`, ayant un rayon de `#!py 200` pixels et partant de la gauche pour aller jusqu'à la droite. Cet arc est donc tourné vers le bas.

??? question "Groenland :flag_gl:"

    Le drapeau du Groenland est au format 3:2.

    ![Le drapeau groenlandais](images/groenland.svg){width=30% .center}

    Le disque central a un rayon égal au $\frac{1}{3}$ de la hauteur. Son centre est un peu décalé vers la gauche.

    ![Les dimensions du drapeau groenlandais](images/groenland_construction.svg){width=40% .center}

    {{ IDE('./pythons/groenland', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('groenland',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

## Avec des polygones

On rappelle l'instruction `lignes` :

???+ tip "Tracer des `#!py lignes`"

    Trace une suite de segments sur la figure.

    Il faut fournir les coordonnées `x` et `y` des différents points, à la suite les uns des autres.

    Si l'on a réglé la couleur de coloriage, la zone entourée par les lignes est coloriée.

    Par exemple, `#!py lignes(-30, 50, 20, 10, 100, 30)` trace la ligne :

    * partant de `#!py -30, 50`,
    * passant par `#!py 20, 10`,
    * se terminant en `#!py 100, 30`.

Utiliser cette instruction afin de construire ces derniers drapeaux :

??? question "Seychelles :flag_sc:"

    Le drapeau des Seychelles est au format 2:1.

    ![Le drapeau des Seychelles](images/seychelles.svg){width=30% .center}

    Les bandes bleu, jaune et rouge coupent la largeur en trois portions égales.

    Les bandes rouge, blanche et verte coupent la hauteur en trois portions égales.

    {{ IDE('./pythons/seychelles', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('seychelles',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}
    
??? question "Guyana :flag_gy:"

    Le drapeau du Guyana est au format 5:3.

    ![Le drapeau du Guyana](images/guyana.svg){width=30% .center}

    Pouvez-vous le tracer en seulement trois instructions de tracé (sans compter les instructions de paramétrage des couleurs ) ? 

    {{ IDE('./pythons/guyana', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('guyana',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}
