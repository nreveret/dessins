---
author: Nicolas Revéret
title: Tortue
---

# :turtle: Le mode *tortue*

Dans ce mode, on imagine qu'une tortue tient le crayon ! Pour dessiner, il faut alors tout lui dire : « *avance de 100 pixels* », « *tourne à droite de 45°* », *etc*.

??? note "Les instructions de la :turtle:"

    ??? tip "`avance` de ..."

        Avance et trace un trait.

        Il faut fournit un seul paramètre : la longueur `pixels` (un nombre entier).

        Par exemple, `#!py avance(152)` fait avancer le crayon de 152 pixels.

    ??? tip "Tourne vers la `gauche` de ..."

        Tourne à gauche d'un certain nombre de degrés.

        Il faut fournit un seul paramètre : l'angle `degres`.

        Initialement le crayon point vers la gauche. On tourne dans le sens inverse des aiguille d'une montre (90 est donc en haut).

        Par exemple, `#!py gauche(90)` tourne le crayon de 90° vers la gauche. S'il pointait vers la droite, il pointe désormais vers le haut.
        

    ??? tip "Tourne vers la `droite` de ..."

        Tourne à droite d'un certain nombre de degrés.

        Il faut fournit un seul paramètre : l'angle `degres`.

        Initialement le crayon point vers la gauche. On tourne dans le sens inverse des aiguille d'une montre (90 est donc en haut).

        Par exemple, `#!py droite(90)` tourne le crayon de 90° vers la droite. S'il pointait vers la droite, il pointe désormais vers le bas.


    ??? tip "`saute` jusqu'à ..."

        Amène ne crayon à la position x, y **sans dessiner**.

        Il faut fournit deux paramètres :
        
        * l'abscisse `x` du point d'arrivée,
        * l'ordonnée `y` du point d'arrivée.

        Par exemple, `#!py saute(-20, 100)` amène le crayon en `x = -20, y = 100`.


    ??? tip "`oriente_vers` ..."

        Oriente la tortue vers l'angle fournit en paramètres.

        Il faut fournir l'angle (en degrés) en paramètre.
        
        Par exemple, `#!py oriente_vers(90)` oriente la tortue vers le haut.


    ??? tip "`debut_coloriage`"

        Débute une séquence de coloriage avec la tortue. Tous les tracés entre l'appel de cette fonction et 
        celui de `fin_coloriage` formeront une unique forme coloriée.

        Il n'y a pas de paramètre. La couleur du coloriage est réglée avec `couleur_coloriage`.

        Par exemple, `#!py debut_coloriage()` débute une séquence de coloriage.


    ??? tip "`fin_coloriage`"

        Termine une séquence de coloriage avec la tortue.

        Il n'y a pas de paramètre.
        
        Par exemple, `#!py fin_coloriage()` termine la séquence de coloriage.


    ??? tip "`position`"

        Renvoie la position de la tortue sous la forme d'un couple de coordonnées `#!py (x, y)`.

        Il n'y a pas de paramètre.


    ??? tip "`orientation`"

        Renvoie l'orientation de la tortue.

        Il n'y a pas de paramètre.


## Prise en main

??? question "Rectangle"

    Dessinez, à l'aide de la tortue, un rectangle de 150 pixels de large et 75 de haut.

    Le rectangle aura un contour `"DarkBlue"` et sera colorié en `"Aquamarine"`.
    
    {{ IDE('./pythons/rectangle', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('rectangle',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Triangle équilatéral"

    Dessinez, à l'aide de la tortue, un triangle équilatéral de 200 pixels de côté.

    Son contour sera rouge.

    {{ IDE('./pythons/triangle_equi', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('triangle_equi',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Pyramides"

    Reproduisez la figure ci-dessous :
    
    ![Pyramides](images/triangles.svg){.center width=50%}

    Le ciel a pour couleur `"Skyblue"`, le sable est `"yellow"` et les pyramides `"brown"`.
    
    Les pyramides sont des triangles isocèles rectangles (leurs angles mesurent 90°, 45° et 45°).
    
    Les côtés de l'angle droit :
    
    * de la première pyramide mesurent 100 pixels,
    
    * de la deuxième mesurent 50 pixels,
    
    * de la troisième mesurent 25 pixels.

    !!! tip "Astuces"

        Vous pouvez utiliser la fonction `rectangle` pour tracer le ciel et le sable.

        Mettez la couleur du trait à `"transparent"` pour ne pas avoir de traits noirs.
  
    {{ IDE('./pythons/pyramides', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('pyramides',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}
  
## Figures complexes

??? question "Hexagone"

    Reproduisez la figure ci-dessous :
    
    ![Hexagone](images/hexagone.svg){.center width=50%}

    Tous les triangles sont équilatéraux et font 100 pixels de côtés.

    Ils ont pour couleurs (dans cet ordre en partant de la droite):

    * `"red"`,
    * `"orange"`,
    * `"yellow"`,
    * `"lightgreen"`,
    * `"skyblue"`,
    * `"purple"`.

    {{ IDE('./pythons/hexagone', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('hexagone',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Assiettes"

    Reproduisez la figure ci-dessous :
    
    ![Marches](images/marches.svg){.center width=50%}

    Toutes les assiettes sont des segments de 10 pixels de large.

    L'assiette rouge fait 200 pixels de long, la orange 180 pixels de long, la jaune 160 pixels de long *etc*.

    Elles ont pour couleurs (dans cet ordre en partant du bas):

    * `"red"`,
    * `"orange"`,
    * `"yellow"`,
    * `"lightgreen"`,
    * `"skyblue"`,
    * `"purple"`,
    * `"purple"`,
    * `"pink"`,
    * `"black"`.

    ??? tip "Astuces"

        Vous pouvez répéter les actions suivantes :

        * régler la couleur du trait,
        * sauter au bon endroit,
        * avancer de la bonne longueur,
        * recommencer !

    {{ IDE('./pythons/assiettes', MODE="delayed_reveal", MAX=10, TERM_H=2, MIN_SIZE= 10)}}
    
    {{ figure('assiettes',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}
