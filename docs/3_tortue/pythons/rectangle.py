# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)

largeur_trait(5)
couleur_trait(...)
couleur_coloriage(...)

debut_coloriage()
avance(...)
gauche(...)
fin_coloriage()
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)

largeur_trait(5)
couleur_trait("DarkBlue")
couleur_coloriage("Aquamarine")

debut_coloriage()
avance(150)
gauche(90)
avance(75)
gauche(90)
avance(150)
gauche(90)
avance(75)
fin_coloriage()
# --- PYODIDE:post --- #
affiche("rectangle")
