# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)

largeur_trait(1)
couleur_trait("transparent")

# Le premier « quartier »
couleur_coloriage("red")
debut_coloriage()
avance(...)
gauche(...)
avance(...)
gauche(...)
avance(...)
fin_coloriage()

# Il en reste 5 à dessiner !
...
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)

largeur_trait(1)
couleur_trait("transparent")

couleur_coloriage("red")
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()

couleur_coloriage("orange")
gauche(180)
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()

couleur_coloriage("yellow")
gauche(180)
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()

couleur_coloriage("lightgreen")
gauche(180)
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()

couleur_coloriage("skyblue")
gauche(180)
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()

couleur_coloriage("purple")
gauche(180)
debut_coloriage()
avance(100)
gauche(120)
avance(100)
gauche(120)
avance(100)
fin_coloriage()
# --- PYODIDE:post --- #
affiche("hexagone")
