# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)

largeur_trait(5)
couleur_trait(...)

avance(...)
gauche(...)
...
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)

couleur_trait("transparent")
couleur_coloriage("Skyblue")
rectangle(-300, 0, 600, 200)
couleur_coloriage("yellow")
rectangle(-300, -200, 600, 200)

largeur_trait(5)
couleur_trait("brown")
couleur_coloriage("brown")

debut_coloriage()
gauche(45)
avance(100)
droite(90)
avance(100)
gauche(90)
avance(50)
droite(90)
avance(50)
gauche(90)
avance(25)
droite(90)
avance(25)
saute(0, 0)
fin_coloriage()
# --- PYODIDE:post --- #
affiche("pyramides")
