# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)

largeur_trait(5)
couleur_trait(...)

avance(...)
gauche(...)
...
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)

largeur_trait(5)
couleur_trait("red")

avance(200)
gauche(120)
avance(200)
gauche(120)
avance(200)
# --- PYODIDE:post --- #
affiche("triangle_equi")
