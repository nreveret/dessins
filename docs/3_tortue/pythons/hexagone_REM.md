Ce code est très répétitif. Avec un peu d'habitude, on peut le simplifier :

```python
nouveau_dessin(600, 400)

largeur_trait(1)
couleur_trait("transparent")
couleurs = ["red", "orange", "yellow", "lightgreen", "skyblue", "purple"]

for c in couleurs:
    couleur_coloriage(c)
    debut_coloriage()
    for cote in range(3):
        avance(100)
        gauche(120)
    fin_coloriage()
    gauche(60)
```