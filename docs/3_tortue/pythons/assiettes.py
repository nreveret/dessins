# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)
largeur_trait(10)

# Assiette rouge
couleur_trait("red")
saute(-200, 0)
avance(400)

# Il en reste 8 à dessiner !
...
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)
largeur_trait(10)

# Assiette rouge
couleur_trait("red")
saute(-200, 0)
avance(400)
# Assiette orange
couleur_trait("orange")
saute(-190, 10)
avance(380)
# Assiette jaune
couleur_trait("yellow")
saute(-180, 20)
avance(360)
# Assiette verte
couleur_trait("lightgreen")
saute(-170, 30)
avance(340)
# Assiette bleue
couleur_trait("skyblue")
saute(-160, 40)
avance(320)
# Assiette violette
couleur_trait("purple")
saute(-150, 50)
avance(300)
# Assiette rose
couleur_trait("pink")
saute(-140, 60)
avance(280)
# Assiette noire
couleur_trait("black")
saute(-130, 70)
avance(260)
# --- PYODIDE:post --- #
affiche("assiettes")
