---
author: Nicolas Revéret
title: Accueil
---

# 🏡 Accueil

Ce site propose de découvrir la programmation en réalisant des dessins avec Python.

=== "Un"

    ![Un joli cercle](images/cercles_1.svg){.autolight width=40% .center}

=== "Deux"

    ![Des jolis cercles](images/cercles_2.svg){.autolight width=40% .center}

=== "Trois"

    ![Des jolis cercles](images/cercles_3.svg){.autolight width=40% .center}

=== "Quatre"

    ![Des jolis cercles](images/cercles_4.svg){.autolight width=40% .center}

=== "Huit !"

    ![Des jolis cercles](images/cercles_8.svg){.autolight width=40% .center}


Il est divisé en plusieurs parties. Il n'est pas utile de toutes les traiter.

* [🏃🏽 Premiers pas](1_depart/1_premiers_pas.md) : on y apprend les premières instructions ;

* [🚩 Tracé de drapeaux](2_drapeaux/2_drapeaux.md) : des lignes, des rectangles, des cercles et des arcs ;

* [:turtle: Mode *tortue*](3_tortue/3_tortue.md) : on y rencontre une tortue artiste-peintre !

* [🤖 Répétitions](4_repetitions/4_repetitions.md) : on automatise les constructions.

Une aide est proposée [sur cette page 🧐](aide/aide.md).

Vous pouvez aussi faire des constructions libres dans le [Bac à sable 👷🏽](5_bac_a_sable/5_bac_a_sable.md).

Les crédits sont [ici 👍🏽](credits.md).
