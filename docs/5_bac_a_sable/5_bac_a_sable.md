---
author: Nicolas Revéret
title: Bac à sable
---

# 👷🏽 Bac à sable

Cette page vous est réservée.

Vous pouvez dessiner ce que vous voulez !

{{ IDE('./pythons/bac_a_sable', MODE="no_valid", TERM_H=2, MIN_SIZE= 10)}}

{{ figure('bac_a_sable',
    div_class='py_mk_figure center',
    inner_text= 'Votre figure sera ici',
    admo_kind='???+',
    admo_class='tip',
    admo_title='Votre figure',
) }}