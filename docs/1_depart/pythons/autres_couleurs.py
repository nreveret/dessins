# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(400, 300)
couleur_trait("red")       # <-- ici...
couleur_coloriage("blue")  # <-- et là !
rectangle(0, 0, 100, 40)
# --- PYODIDE:corr --- #
nouveau_dessin(400, 300)
couleur_trait("blue")
couleur_coloriage("red")
rectangle(0, 0, 100, 40)

# --- PYODIDE:post --- #
affiche("autres_couleurs")
