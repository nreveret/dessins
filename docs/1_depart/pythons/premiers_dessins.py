# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(400, 300)  # l'image fait 400 de large et 300 de haut
couleur_trait("red")
couleur_coloriage("blue")
# le coin BG du rectangle est au centre de la figure et fait 100 sur 40
rectangle(0, 0, 100, 40)
# --- PYODIDE:post --- #
affiche("premiers_dessins")
