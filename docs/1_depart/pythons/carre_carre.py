# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(400, 300)
couleur_trait("red")
couleur_coloriage("blue")
rectangle(0, 0, 100, 40)
# --- PYODIDE:corr --- #
nouveau_dessin(400, 300)
couleur_coloriage("yellow")
rectangle(-50, -50, 50, 50)
couleur_coloriage("purple")
rectangle(0, 0, 100, 100)
# --- PYODIDE:post --- #
affiche("carre_carre")
