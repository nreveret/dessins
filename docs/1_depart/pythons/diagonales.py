# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(600, 400)
couleur_trait(...)
largeur_trait(5)
# --- PYODIDE:corr --- #
nouveau_dessin(600, 400)
couleur_trait("red")
largeur_trait(5)
ligne(-300, 200, 300, -200)
ligne(-300, -200, 300, 200)
# --- PYODIDE:post --- #
affiche("diagonales")
