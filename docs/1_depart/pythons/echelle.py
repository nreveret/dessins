# --- PYODIDE:env --- #
import drawsvg as draw
from dessin_svg import *

# --- PYODIDE:code --- #
nouveau_dessin(..., ...)
couleur_trait(...)
largeur_trait(...)
...
# --- PYODIDE:corr --- #
nouveau_dessin(50, 700)
couleur_trait("brown")
largeur_trait(5)
ligne(-25, -350, -25, 350)
ligne(+25, -350, 25, 350)
ligne(-25, -275, 25, -275)
ligne(-25, -200, 25, -200)
ligne(-25, -125, 25, -125)
ligne(-25, -50, 25, -50)
ligne(-25, 25, 25, 25)
ligne(-25, 100, 25, 100)
ligne(-25, 175, 25, 175)
ligne(-25, 250, 25, 250)
ligne(-25, 325, 25, 325)
# --- PYODIDE:post --- #
affiche("echelle")
