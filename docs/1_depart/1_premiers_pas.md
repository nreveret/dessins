---
author: Nicolas Revéret
title: Premiers pas
---

# 🏃🏽 Premiers pas

Le but de ce site est de faire des dessins. Tout simplement.

Mais avant d'être Léonard, commençons par apprendre les bases !

## Les instructions

Nous allons dessiner en utilisant **la programmation** avec le langage `Python` 🐍.

Pour cela il va falloir saisir des **instructions** et demander à `Python` de les **exécuter**.

???+ danger "Attention"

    `Python` n'est pas *intelligent* comme vous : il arrête de lire à la première erreur ou faute d'orthographe. 
    Il faut donc faire **très attention** à ce que l'on tape.
    
    Sans trop s'inquiéter tout de même, la pire chose qui puisse arriver est d'obtenir un *bug* 🐛 !

Ouvrez le menu ci-dessous afin d'y trouver un *éditeur*. Il contient déjà des instructions.

??? question "Première figure"

    Cliquez sur le bouton "*Lancer*" afin d'afficher la figure.

    Cela peut prendre un peu de temps lors de la première exécution... 

    {{ IDE('./pythons/premiers_dessins', MODE="no_valid", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('premiers_dessins',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "D'autres couleurs"

    Modifiez le code afin que le rectangle soit colorié en rouge et ait une bordure bleue.

    {{ IDE('./pythons/autres_couleurs', MODE="delayed_reveal", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('autres_couleurs',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Rectangle ➞ Carré"

    Dessinez un carré jaune.

    {{ IDE('./pythons/rectangle_carre', MODE="delayed_reveal", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('rectangle_carre',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Carré $\times 2$"

    Dessinez deux carrés **sur le même dessin **:

    * un petit jaune,

    * un grand violet.

    {{ IDE('./pythons/carre_carre', MODE="delayed_reveal", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('carre_carre',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}
    
## Les coordonnées

Il est facile de se repérer dans la figure, il suffit de fournir deux nombres `x` et  `y` (les « matheux » disent *abscisse* et *ordonnée*) :

* la coordonnée `x` désigne la position horizontale (de gauche à droite),

* la coordonnée `y` désigne la position verticale (de bas en haut),

* le point de coordonnées `x = 0` et `y = 0` est ... au centre de la figure !

![Le repère](../1_depart/images/repere.png){ .autolight .py_mk_figure center width=60%}

???+ warning "Hors-champ"

    Si la figure fait 500 pixels de large, comme le repère est centré au milieu, cela signifie que l'on peut dessiner entre -250 et 250.

    Si l'on sort de la figure, les pixels ne sont pas affichés...

Entraînons-nous sur ces coordonnées en introduisant une nouvelle instruction : la `ligne`.

???+ tip "Tracer une `ligne`"

    Trace un segment sur la figure.

    Il faut fournir quatre paramètres :

    * `x_debut` : l'abscisse du point de départ ;
    * `y_debut` : l'ordonnée du point de départ ;
    * `x_fin` : l'abscisse du point d'arrivée ;
    * `y_fin` : l'ordonnée du point de départ.

    On peut régler la largeur du trait ou sa couleur.

    Par exemple, `#!py ligne(-30, 50, 20, 10)` trace une ligne depuis le point de coordonnées `#!py x = -30` et `#!py y = 50` vers celui de coordonnées `#!pyx = 20` et `#!py y = 10`.


??? question "Diagonales"

    On donne une figure de 600 pixels de large et 400 de haut.

    Tracer en rouge et avec un épaisseur de 5 pixels les deux diagonales de l'image.

    ??? tip "Coup de pouce"

        Le coin en haut à gauche de l'image a pour coordonnées $(-300~;~200)$. Le coin en bas à droite $(300~;~-200)$.

        le tracé de la diagonale correspondante se fait donc en saisissant l'instruction `#!py ligne(-300, 200, 300, -200)`.

    {{ IDE('./pythons/diagonales', MODE="delayed_reveal", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('diagonales',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Échelle"

    Pouvez-vous dessiner une échelle ?
    
    L'image doit faire 50 pixels de large et 700 de haut.

    Le trait de l'échelle fait 5 pixels de large et il est marron.

    L'échelle est debout, les barreaux font 50 pixels de large et ils sont placés tous les 75 pixels en partant du bas de l'image.

    Bien entendu, il n'y pas de barreau au sol !

    {{ IDE('./pythons/echelle', MODE="delayed_reveal", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('echelle',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}

??? question "Maison 🏠"

    Pouvez-vous dessiner une maison ?
    
    Cette fois-ci, vous avez carte blanche : votre maison peut être carré ou rectangulaire, avec un toit plat ou à deux pentes... Vous êtes l'architecte !

    On fournit une nouvelle instructions pour vous aider : les `lignes`. Cela peut être pratique pour dessiner un toit colorié !

    ??? tip "Tracer des `lignes`"

        Trace une suite de segments sur la figure.

        Il faut fournir les coordonnées `x` et `y` des différents points, à la suite les uns des autres.

        Si l'on a réglé la couleur de coloriage, la zone entourée par les lignes est coloriée.

        Par exemple, `#!py lignes(-30, 50, 20, 10, 100, 30)` trace la ligne :

        * partant de `-30, 50`,
        * passant par `20, 10`,
        * se terminant en `100, 30`.

    {{ IDE('./pythons/maison', MODE="no_valid", TERM_H=2, MIN_SIZE= 10)}}

    {{ figure('maison',
        div_class='py_mk_figure center',
        inner_text= 'Votre figure sera ici',
        admo_kind='???+',
        admo_class='tip',
        admo_title='Votre figure',
    ) }}